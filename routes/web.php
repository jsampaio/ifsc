<?php

use App\Http\Controllers\UsuariosController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Rota Raiz
Route::get('/', function () {
    return view('home');
});
#Rotas Middleware
Route::middleware('auth')->group(function(){  
#rotas usuário
    Route::get('/usuario/novo', [UsuariosController::class, 'novo'])->name('usuario_novo');
    Route::post('/usuario/inserir', [UsuariosController::class, 'inserir'])->name('usuario_inserir');    
    Route::get('/usuario/editar/{id}', [UsuariosController::class, 'editar'])->name('usuario_editar');
    Route::post('/usuario/alterar/{id}', [UsuariosController::class, 'alterar'])->name('usuario_alterar');
    Route::get('/usuario/excluir/{id}', [UsuariosController::class, 'excluir'])->name('usuario_excluir');    
    Route::get('/usuario/lista', [UsuariosController::class, 'telaCadastros'])->name('usuario_lista');
#rotas pedido  
    Route::get('/pedido/editar/{id}', [UsuariosController::class, 'editarPedido'])->name('pedido_editar');
    Route::post('/pedido/alterar/{id}', [UsuariosController::class, 'alterarPedido'])->name('pedido_altrar');
    Route::get('/pedido/excluir/{id}', [UsuariosController::class, 'excluirPedido'])->name('pedido_excluir');
    Route::get('/pedido/fechar/{id}', [UsuariosController::class, 'fecharPedido'])->name('fechar_pedido');
    Route::post('/pedido/inserir', [UsuariosController::class, 'inserirPedido'])->name('inserir_pedido');    
    Route::get('/pedido/lista', [UsuariosController::class, 'retornoPedido'])->name('pedido_lista');    
    Route::get('/pedido/novo', [UsuariosController::class, 'pedidoNovo'])->name('pedido_novo');
});
#Rotas Login
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
# Rotas Usuário
# Rotas Pedido
