@extends('layouts.app')

@if (session('mensagem'))
    <div class="alert alert-success">
        {{session('mensagem')}}
    </div>
@endif

@section('content')
    <h2 class="mb-5">Pedido</h2>
    <form method="POST" action="{{route('inserir_pedido')}}">
        @csrf
        
        <div class="form-group row mb-3">
            <label for="inputProduto" class="col-md-3 text-md-right col-form-label">Produto</label>
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="produto" id="inputProduto" placeholder="Whey Protein">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="inputValor" class="col-md-3 text-md-right col-form-label">Valor</label>            
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="valor" id="inputValor" placeholder="300.00">
            </div>           
        </div> 
        
        <div class="form-group row mb-3">
            <label for="inputFrete" class="col-md-3 text-md-right col-form-label">Frete</label>
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="frete" id="inputFrete" placeholder="19.90">
            </div>
        </div>

        <input type="submit" class="btn btn-primary" value="Realizar Pedido">
        <a href="{{ route('logout')}}" class="btn btn-primary">Logout</a>
    </form>

@endsection('conteudo')