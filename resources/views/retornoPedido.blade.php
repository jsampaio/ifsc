@extends('layouts.app')

@section('content')

<h2 class="mb-5">Pedidos</h2>

@if (session('mensagem'))
    <div class="alert alert-success">
        {{session('mensagem')}}
    </div>
@endif

<table class="table table-striped table-hover">
    <tr>
        <th>ID</th>
        <th>Produto</th>
        <th>Valor</th>
        <th>Frete</th>
        <th>Data Criação</th>
        <th>Última Alteração</th>        
        <th>Alterar</th>
        <th>Excluir</th>
        <th>Finalizar</th>
    </tr>
    @foreach($pedido as $p)
    <tr>
        <td>{{$p->id}}</td>
        <td>{{$p->produto}}</td>
        <td>{{$p->valor}}</td>
        <td>{{$p->frete}}</td>
        <td>{{$p->created_at}}</td>
        <td>{{$p->updated_at}}</td>        
        <td>
            <a 
                href="{{ route('pedido_editar', ['id' => $p->id]) }}" 
                class="btn btn-primary">Alterar
            </a>
        </td>
        <td>
            <a 
                href="#" 
                onclick="excluir({{$p->id}})"
                class="btn btn-danger">Excluir
            </a>
        </td>
        <td>        
            <a                
                href="{{ route('fechar_pedido', ['id' => $p->id]) }}"
                class="btn btn-success">Fechar
            </a>
        </td>
    </tr>
    @endforeach
</table>
<a href="{{ route('pedido_novo') }}" class="btn btn-primary">Novo Pedido</a>
<a href="{{ route('logout')}}" class="btn btn-primary">Logout</a>

<script javascript>
    function excluir(id){
        if (confirm('Você deseja realmente excluir o pedido de id: ' + id + '?')) {
           location.href = route('pedido_excluir', { id: id});
        }        
    }
</script>

@endsection