<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Cliente;
use App\Models\Pedido;
use App\Models\Usuario;
use Error;
use ErrorException;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\PseudoTypes\True_;
use Symfony\Component\CssSelector\Exception\InternalErrorException;

class UsuariosController extends Controller
{
    function exibeLogin()
    {
        return view('auth.login');
    }
    function inserir(Request $req)
    {

        $usuario = new Usuario;
        $usuario->nome = $req->input('nome');
        $usuario->email = $req->input('email');
        $usuario->senha = $req->input('senha');
        $usuario->endereco = $req->input('endereco');
        $usuario->cep = $req->input('cep');
        $usuario->cidade = $req->input('cidade');
        $usuario->estado = $req->input('estado');
        $usuario->id_categoria = $req->input('categoria');
        $usuario->save();

        $cliente = new Cliente;
        $cliente->nome = $req->input('nome');
        $cliente->email = $req->input('email');
        $cliente->senha = $req->input('senha');
        $cliente->endereco = $req->input('endereco');
        $cliente->cep = $req->input('cep');
        $cliente->cidade = $req->input('cidade');
        $cliente->estado = $req->input('estado');
        $cliente->save();

        session()->flash('mensagem', "O usuário {$usuario->nome} foi cadastrado com sucesso");
        return redirect()->route('usuario_lista', ['usuario' => $usuario->id]);
    }
    function telaCadastros()
    {
        $usuarios = Usuario::all();
        return view('retornoLogin', [
            'usuarios' => $usuarios
        ]);
    }

    function novo()
    {
        $categorias = Categoria::all();
        return view('usuarioNovo', ['categorias' => $categorias]);
    }
    function editar($id)
    {
        $usuario = Usuario::findOrFail($id);
        $categorias = Categoria::all();
        return view('usuarioEditar', ['usuario' => $usuario, 'categorias' => $categorias]);
    }
    function alterar(Request $req, $id)
    {
        $usuario = Usuario::findOrFail($id);
        $usuario->nome = $req->input('nome');
        $usuario->email = $req->input('email');
        $usuario->senha = $req->input('senha');
        $usuario->endereco = $req->input('endereco');
        $usuario->cep = $req->input('cep');
        $usuario->cidade = $req->input('cidade');
        $usuario->estado = $req->input('estado');
        $usuario->save();

        session()->flash('mensagem', "O usuário {$usuario->nome} foi alterado com sucesso");
        return redirect()->route('usuario_lista');
    }
    function excluir($id)
    {
        $usuario = Usuario::findOrFail($id);
        $usuario->delete();
        return redirect()->route('usuario_lista');
    }
    function pedidoNovo()
    {
        return view('pedido');
    }
    function retornoPedido()
    {
        $pedidos = Pedido::all();
        return view('retornoPedido', [
            'pedido' => $pedidos
        ]);
    }
    function inserirPedido(Request $req)
    {

        $cliente = Cliente::all();
        $cliente->get('id');
        foreach ($cliente as $value) {
            $user_id = $value['id'];
        }
        $pedido = new Pedido;
        $pedido->produto = $req->input('produto');
        $pedido->valor = $req->input('valor');
        $pedido->frete = $req->input('frete');
        $pedido->id_cliente = $user_id;
        $pedido->save();
        session()->flash('mensagem', "O pedido {$pedido->id} foi realizado com sucesso");
        return redirect()->route('pedido_lista');
    }
    function editarPedido($id)
    {
        $pedido = Pedido::findOrFail($id);
        return view('alterarPedido', ['pedido' => $pedido]);
    }
    function alterarPedido(Request $req, $id)
    {
        $pedido = Pedido::findOrFail($id);
        $pedido->produto = $req->input('produto');
        $pedido->valor = $req->input('valor');
        $pedido->frete = $req->input('frete');
        $pedido->save();

        session()->flash('mensagem', "O pedido {$pedido->id} foi alterado com sucesso");
        return redirect()->route('pedido_lista');
    }
    function confirmaPedido()
    {
        return view('alterarPedido');
    }
    function excluirPedido($id)
    {
        $pedido = Pedido::findOrFail($id);
        $pedido->delete();
        session()->flash('mensagem', "O pedido {$pedido->id} foi fechado com sucesso");
        return redirect()->route('pedido_lista');
    }
    function fecharPedido($id)
    {
        $pedido = Pedido::findOrFail($id);
        $valores = $pedido::where('id', '=', $pedido->id)->get(['valor', 'frete']);
        $valorTotal = 0;
        foreach ($valores as $value) {
            $valorTotal = $value['valor'] + $value['frete'];
        }
        return view('fecharPedido', ['pedido' => $pedido, 'valorTotal' => $valorTotal]);
    }
}
