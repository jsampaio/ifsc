<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model
{
    use SoftDeletes;
    
    protected $table = 'pedido';

    protected $dates = [
        'deleted_at',
        'pedido'
    ];

    function cliente(){
        return $this->belongsTo(Cliente::class, 'id_cliente', 'id');
    }
}
