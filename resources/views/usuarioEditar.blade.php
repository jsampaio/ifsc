@extends('layouts.app')

@section('content')
    <h2 class="mb-5">Alterar Usuário</h2>
    <form method="POST" action="{{ route('usuario_alterar', ['id' => $usuario->id])}}">
        @csrf

        <div class="form-group row mb-3">            
            <label for="inputName" class="col-md-3 text-md-right col-form-label">Nome</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="nome" id="inputName" placeholder="Nome" value="{{$usuario->nome}}">
            </div>
        </div>

        <div class="form-group row mb-3">            
            <label for="inputEmail" class="col-md-3 text-md-right col-form-label">Endereço de email</label>            

            <div class="col-md-6">
                <input type="email" class="form-control" name="email" id="inputEmail" placeholder="nome@exemplo.com.br" value="{{$usuario->email}}">
            </div>
        </div>
        
        <div class="form-group row mb-3">
            
            <label for="inputSenha" class="col-md-3 text-md-right col-form-label">Senha</label>

            <div class="col-md-6">
                <input type="password" class="form-control" name="senha" id="inputSenha" placeholder="Senha" value="{{$usuario->senha}}">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="inputCategoria" class="col-md-3 text-md-right col-form-label">Categoria</label>           

            <div class="col-md-6">
                <select class="form-control" name="categoria" id="inputCategoria">

                    @foreach ($categorias as $k => $cat){
                        <option value="{{ $cat->id }}"
                        {{ ( $cat->id == $usuario->id_categoria ? "selected" :  "") }}>
                        {{ $cat->nome }}
                        </option>
                    }
                    @endforeach
                
                </select>
            </div>
        </div>

        <div class="form-group row mb-3">            
            <label for="inputEndereco" class="col-md-3 text-md-right col-form-label">Endereço</label>            

            <div class="col-md-6">
                <input type="text" class="form-control" name="endereco" id="inputEndereco" placeholder="Endereço" value="{{$usuario->endereco}}">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="inputCep" class="col-md-3 text-md-right col-form-label">CEP</label>            
            
            <div class="col-md-6">
                <input type="text"class="form-control" name="cep" id="inputCep" placeholder="CEP" value="{{$usuario->cep}}">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="inputCidade" class="col-md-3 text-md-right col-form-label">Cidade</label>            

            <div class="col-md-6">
                <input type="text" class="form-control" name="cidade" id="inputCidade" placeholder="Cidade" value="{{$usuario->cidade}}">
            </div>
        </div>
        
        <div class="form-group row mb-5">
            <label for="inputEstado" class="col-md-3 text-md-right col-form-label">Estado</label>

            <div class="col-md-6">
                <select class="form-control" name="estado" value="{{$usuario->estado}}" id="inputEstado">
                    <option selected>Estado</option>
                    <option value="AC">AC</option>
                    <option value="AL">AL</option>
                    <option value="AP">AP</option>
                    <option value="AM">AM</option>
                    <option value="BA">BA</option>
                    <option value="CE">CE</option>            
                    <option value="ES">ES</option>
                    <option value="GO">GO</option>
                    <option value="MA">MA</option>
                    <option value="MT">MT</option>
                    <option value="MS">MS</option>
                    <option value="MG">MG</option>
                    <option value="PA">PA</option>
                    <option value="PB">PB</option>
                    <option value="PR">PR</option>
                    <option value="PE">PE</option>
                    <option value="PI">PI</option>
                    <option value="RJ">RJ</option>
                    <option value="RN">RN</option>
                    <option value="RS">RS</option>
                    <option value="RO">RO</option>
                    <option value="RR">RR</option>
                    <option value="SC">SC</option>
                    <option value="SP">SP</option>
                    <option value="SE">SE</option>
                    <option value="TO">TO</option>
                </select>
            </div>         
        </div>

        <input type="submit" class="btn btn-lg btn-primary" value="Cadastrar">        
        <a href="{{ route('logout')}}" class="btn btn-lg btn-primary">Logout</a>
    </form>
@endsection