@extends('layouts.app')

@section('content')

            <h2 class="m-3">Usuários Cadastrados</h2>

            @if (session('mensagem'))
                <div class="alert alert-success">        
                    {{session('mensagem')}}
                </div>
            @endif

            <table class="table table-striped table-hover">
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Categoria</th>
                    <th>Cidade</th>
                    <th>Alterar</th>
                    <th>Excluir</th>
                </tr>
                @foreach($usuarios as $u)
                <tr>
                    <td>{{$u->id}}</td>
                    <td>{{$u->nome}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->categoria->nome}}</td>
                    <td>{{$u->cidade}}</td>
                    <td>
                        <a 
                            href="{{ route('usuario_editar', ['id' => $u->id]) }}" 
                            class="btn btn-primary">Alterar
                        </a>
                    </td>
                    <td>
                        <a 
                            href="#" 
                            onclick="excluir({{ $u->id}})"
                            class="btn btn-danger">Excluir
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
            <a href="{{ route('usuario_novo')}}" class="btn btn-primary">Cadastrar Novo</a>
            <a href="{{ route('pedido_novo')}}" class="btn btn-primary">Pedido</a>
            <a href="{{ route('logout')}}" class="btn btn-primary">Logout</a>

            <script>
                function excluir(id){
                    if (confirm('Você deseja realmente excluir o usuário de id: ' + id + '?')) {
                    location.href = route('usuario_excluir', { id: id});
                    }        
                }
            </script>
 
@endsection