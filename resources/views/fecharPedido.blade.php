@extends('layouts.app')

@if (session('mensagem'))
    <div class="alert alert-success">
        {{session('mensagem')}}
    </div>
@endif

@section('content')

    <form method="POST">
        @csrf
        <h2 class="mb-5">Fechar do Pedido</h2>

        <div class="form-group row mb-3">
            <label for="inputProduto" class="col-md-3 text-md-right col-form-label">Produto</label>
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="produto" id="inputProduto" placeholder="Whey Protein" value="{{$pedido->produto}}">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="inputValor" class="col-md-3 text-md-right col-form-label">Valor</label>            
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="valor" id="inputValor" placeholder="300,00" value="{{$pedido->valor}}">
            </div>
        </div>

        <div class="form-group row mb-3">
            <label for="inputFrete" class="col-md-3 text-md-right col-form-label">Frete</label>            
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="frete" id="inputFrete" placeholder="19,90" value="{{$pedido->frete}}">
            </div>
        </div>

        <div class="form-group row mb-5">
            <label for="inputTotal" class="col-md-3 text-md-right col-form-label">Total</label>            
            
            <div class="col-md-6">
                <input type="text" class="form-control" name="total" id="inputTotal" placeholder="000.00" value="{{$valorTotal}}">
            </div>
        </div>

        <a href="#" onclick="excluir({{$pedido->id}})" type="submit" class="btn btn-primary">Fechar Pedido</a>
        <a href="{{ route('logout')}}" class="btn btn-primary">Logout</a>
    </form>

@endsection('conteudo')

<script javascript>
    function excluir(id){
        if (confirm('Você deseja realmente fechar o pedido de id: ' + id + '?')) {
           location.href = route('pedido_excluir', { id: id});
        }        
    }
</script>