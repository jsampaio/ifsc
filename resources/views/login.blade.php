@extends('template')


@section('conteudo')
    <div class="position-absolute top-50 start-50 translate-middle text-center">
        <form action="{{route('tenta_login')}}" method="POST">
        @csrf
            <div class="mb-3">
                <label for="inputEmail" class="form-label">Login</label>
                <input type="email" class="form-control" id="inputEmail" name="email">          
            </div>
            <div class="mb-3">
                <label for="inputSenha" class="form-lavel">Senha</label>
                <input type="password" class="form-control" id="inputSenha" name="senha">            
            </div>
            <div>
                 <input type="submit" class="btn btn-primary"  value="Entrar">
            </div>
        </form>    
    </div>    
@endsection